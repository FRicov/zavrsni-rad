import dataUtils.PostcodeDataLoader;
import org.locationtech.jts.geom.*;
import serializer.impl.*;

import java.io.IOException;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        final GeometryFactory geometryFactory = new GeometryFactory();
        final List<Geometry> codedGeometry;
        try {
             codedGeometry = PostcodeDataLoader.loadShapeFile("C:\\Users\\filip\\Desktop\\Završni rad\\Sectors.shp", geometryFactory);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        final Geometry geometry = codedGeometry.get(0);

        final GsonSerializer gsonSerializer = new GsonSerializer();
        final JavaSerializer javaSerializer = new JavaSerializer();
        final KryoSerializer kryoSerializer = new KryoSerializer((geometry).getClass());
        final JacksonSerializer jacksonSerializer = new JacksonSerializer();
        final JacksonAfterburnerSerializer jacksonAfterburnerSerializer = new JacksonAfterburnerSerializer();
        final GeoToolsSerializer geoToolsSerializer = new GeoToolsSerializer(geometryFactory);
        final ApacheSerializer apacheSerializer = new ApacheSerializer();

        System.out.println("Single-threaded test:\n");
        System.out.println("Total Geometry objects: " + codedGeometry.size() + "\n");
        System.out.println("java: " + javaSerializer.test(codedGeometry, 10, false) + "\n");
        System.out.println("apache: " + apacheSerializer.test(codedGeometry, 10,false) + "\n");
        System.out.println("Gson: " + gsonSerializer.test(codedGeometry, 10,false) + "\n");
        System.out.println("kryo: " + kryoSerializer.test(codedGeometry, 10,false) + "\n");
        System.out.println("jackson: " + jacksonSerializer.test(codedGeometry, 10,false) + "\n");
        System.out.println("jackson + afterburner: " + jacksonAfterburnerSerializer.test(codedGeometry, 10,false) + "\n");
        System.out.println("geotools: " + geoToolsSerializer.test(codedGeometry, 10,false) + "\n");


        System.out.println("Multi-threaded test:\n");
        System.out.println("java: " + javaSerializer.test(codedGeometry, 10, true) + "\n");
        System.out.println("apache: " + apacheSerializer.test(codedGeometry, 10,true) + "\n");
        System.out.println("Gson: " + gsonSerializer.test(codedGeometry, 10,true) + "\n");
        System.out.println("jackson: " + jacksonSerializer.test(codedGeometry, 10,true) + "\n");
        System.out.println("jackson + afterburner: " + jacksonAfterburnerSerializer.test(codedGeometry, 10,true) + "\n");
        System.out.println("geotools: " + geoToolsSerializer.test(codedGeometry, 10,true) + "\n");
    }
}
