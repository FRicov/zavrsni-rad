/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataUtils;

import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.store.ContentFeatureCollection;
import org.geotools.data.store.ContentFeatureSource;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.precision.GeometryPrecisionReducer;
import org.opengis.feature.simple.SimpleFeature;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;


/**
 * @author Krešimir Pripužić <kresimir.pripuzic@fer.hr>
 */
public class PostcodeDataLoader {
    public static List<Geometry> loadShapeFile(String filePath, GeometryFactory gf) throws NoSuchElementException, IOException {
        //sectors
        List<Geometry> result = new LinkedList<>();
        File file = new File(filePath);
        ShapefileDataStore dataStore = new ShapefileDataStore(file.toURI().toURL());
        ContentFeatureSource featureSource = dataStore.getFeatureSource();
        ContentFeatureCollection featureCollection = featureSource.getFeatures();
        try (SimpleFeatureIterator iterator = featureCollection.features()) {
            while (iterator.hasNext()) {
                SimpleFeature feature = iterator.next();
                GeometryPrecisionReducer gpr = new GeometryPrecisionReducer(gf.getPrecisionModel());
                Geometry originalGeometry = (Geometry) feature.getDefaultGeometry();
                //reducing the number of decimal digits
                Geometry geometry = gpr.reduce(originalGeometry);
                result.add(geometry);
            }
        }
        dataStore.dispose();
        return result;
    }
}
