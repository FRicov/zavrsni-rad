package serializer.impl;

import org.locationtech.jts.geom.Geometry;
import serializer.AbstractSerializer;

import java.io.*;


public class JavaSerializer extends AbstractSerializer{

    @Override
    public byte[] serialize(final Geometry o) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream out = new ObjectOutputStream(bos);
        out.writeObject(o);
        byte[] bytes = bos.toByteArray();
        bos.close();
        return bytes;
    }

    @Override
    public Geometry deserialize(final byte[] s) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bis = new ByteArrayInputStream(s);
        ObjectInput in = new ObjectInputStream(bis);
        Object o = in.readObject();
        bis.close();
        return (Geometry) o;
    }
}
