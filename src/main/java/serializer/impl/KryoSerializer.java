package serializer.impl;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.util.DefaultInstantiatorStrategy;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;
import org.locationtech.jts.geom.impl.CoordinateArraySequenceFactory;
import org.objenesis.strategy.StdInstantiatorStrategy;
import serializer.AbstractSerializer;

public class KryoSerializer extends AbstractSerializer {

    Kryo kryo = new Kryo();
    Class<? extends Geometry> type;

    public KryoSerializer(Class<? extends Geometry> type) {
        this.type = type;
        kryo.register(Polygon.class);
        kryo.register(Polygon[].class);
        kryo.register(MultiPolygon.class);
        kryo.register(Geometry.class);
        kryo.register(Envelope.class);
        kryo.register(LinearRing.class);
        kryo.register(GeometryFactory.class);
        kryo.register(CoordinateArraySequenceFactory.class);
        kryo.register(PrecisionModel.class);
        kryo.register(PrecisionModel.Type.class);
        kryo.register(LinearRing[].class);
        kryo.register(CoordinateArraySequence.class);
        kryo.register(Coordinate[].class);
        kryo.register(Coordinate.class);
        kryo.setInstantiatorStrategy(new DefaultInstantiatorStrategy(new StdInstantiatorStrategy()));
    }

    @Override
    public byte[] serialize(final Geometry o) {
        Output output = new Output(1024,786432);
        kryo.writeObject(output, o);
        final byte[] buffer = output.getBuffer();
        output.close();
        return buffer;
    }

    @Override
    public Geometry deserialize(final byte[] s) {
        Input input = new Input(s);
        final Geometry g = kryo.readObject(input, type);
        input.close();
        return g;
    }

}
