package serializer.impl;

import org.apache.commons.lang3.SerializationUtils;
import org.locationtech.jts.geom.Geometry;
import serializer.AbstractSerializer;

public class ApacheSerializer extends AbstractSerializer {

    @Override
    public byte[] serialize(final Geometry o) {
        return SerializationUtils.serialize(o);
    }

    @Override
    public Geometry deserialize(final byte[] s) {
        return SerializationUtils.deserialize(s);
    }
}
