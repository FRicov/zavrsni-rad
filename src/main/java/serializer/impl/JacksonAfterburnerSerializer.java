package serializer.impl;

import com.bedatadriven.jackson.datatype.jts.JtsModule;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.module.afterburner.AfterburnerModule;
import org.locationtech.jts.geom.Geometry;
import serializer.AbstractSerializer;

import java.io.IOException;

public class JacksonAfterburnerSerializer extends AbstractSerializer {

    ObjectMapper objectMapper = new ObjectMapper();

    public JacksonAfterburnerSerializer() {
        this.objectMapper.registerModule(new JtsModule());
        this.objectMapper.registerModule( new AfterburnerModule());
    }

    @Override
    public byte[] serialize(final Geometry o) throws JsonProcessingException {
        return objectMapper.writeValueAsBytes(o);
    }

    @Override
    public Geometry deserialize(final byte[] s) throws IOException {
        return objectMapper.readValue(s, Geometry.class);
    }
}
