package serializer.impl;

import org.geotools.geojson.geom.GeometryJSON;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import serializer.AbstractSerializer;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class GeoToolsSerializer extends AbstractSerializer {

    GeometryJSON geometryJSON;

    public GeoToolsSerializer(final GeometryFactory gf) {
        this.geometryJSON = new GeometryJSON(gf.getPrecisionModel().getMaximumSignificantDigits());
    }

    @Override
    public byte[] serialize(final Geometry o) throws IOException {
        final ByteArrayOutputStream bs = new ByteArrayOutputStream();
        geometryJSON.write(o, bs);
        return bs.toByteArray();
    }

    @Override
    public Geometry deserialize(final byte[] s) throws IOException {
        final ByteArrayInputStream is = new ByteArrayInputStream(s);
        return geometryJSON.read(is);
    }
}
